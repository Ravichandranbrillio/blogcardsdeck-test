/**
 * FunkoBlog - action creator, it handles the emitted action and invokes the wen service to dispatch the payload to reducer
 * Author: Ravichandran P
 */
import {actionTypes} from './types'

/**
 * # fetchBlogArticleAction
 * Purpose: This function is used to handle an action which is dispatched from components
 * Author: Ravichandran P
 * Input Param: url
 * Output Param:
 */
export const fetchBlogArticleAction = (url) => {
    return (dispatch) => {

        dispatch({ type: 'REQUEST_STARTED' })

        return fetchBlogArticleService(url)
            .then(data => {
                if (!data || data["articles"].length === 0)
                    dispatch(setPayload({ payload: null, error: "Error in Fetching" }))
                else if(data && data["articles"].length > 0)
                    dispatch(setPayload({ payload: data["articles"], error: null }))
            })
            .catch((error) => console.log("fetchBlogArticleAction : ",error)
        )
    }
}

/**
 * # setPayload
 * Purpose: This function will be invoked to set the payload, once a web service call is completed
 * Author: Ravichandran P
 * Input Param: dataObj
 * Output Param:
 */
const setPayload = (dataObj) => {

    let lData = dataObj.payload ? dataObj.payload : dataObj.error
    if (lData)
        return {
            type : actionTypes.blogArticleSuccess,
            payload : lData }
    else
        return {
            type : actionTypes.blogArticleFailure,
            payload : lData  }
}

/**
 * # fetchBlogArticleService
 * Purpose: This function will be invoked to consume the web service
 * Author: Ravichandran P
 * Input Param: url
 * Output Param: Promise
 */
const fetchBlogArticleService = (url) => {
    return fetch(url, {
        headers: { 'content-type': 'application/json' },
        method: 'GET'
        }).then(response => response.json())
}

