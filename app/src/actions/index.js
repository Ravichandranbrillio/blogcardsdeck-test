/**
 * index.js - all actions are assigned to an Object and exported
 * Author: Ravichandran P
 */

import * as funkoBlogActions from './FunkoBlog'

export const actionCreators = Object.assign({}, funkoBlogActions)