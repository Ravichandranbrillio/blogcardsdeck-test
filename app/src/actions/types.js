/**
 * index.js - contains the possible Types
 * Author: Ravichandran P
 */

export const actionTypes = {
    blogArticleSuccess: "BLOG_ARTICLE_SUCCESS",
    blogArticleFailure: "BLOG_ARTICLE_FAILURE"
}