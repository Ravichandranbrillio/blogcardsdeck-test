/**
 * FunkoBlog - Reducer, it manages the State for the dispatched payload & returns to connected component
 * Author: Ravichandran P
 */
import {actionTypes} from '../actions/types'

const initialState = {
    type : "",
    response : []
}

/**
 * # funkoBlogArticle
 * Purpose: This function will be invoked to get a new state object based on passed Type & Payload
 * Author: Ravichandran P
 * Input Param: state, action
 * Output Param: newState
 */
export const funkoBlogArticle = (state = initialState, action) => {

    let lActionType =action.type
    let newState = {}

    switch(lActionType) {
        case actionTypes.blogArticleSuccess :
            newState = {...state, type : lActionType, response : action.payload }
            return newState

        case actionTypes.blogArticleFailure :
            newState = {...state, type : lActionType, response : action.payload }
            return newState

        default:
            newState = {...state}
            return newState
    }

}