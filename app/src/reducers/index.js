/**
 * index.js - combines all reducers and export it out
 * Author: Ravichandran P
 */
import { combineReducers } from 'redux'
import {funkoBlogArticle} from './FunkoBlog'

export default combineReducers({
    funkoBlog:funkoBlogArticle,
})