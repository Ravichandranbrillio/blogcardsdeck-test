/**
 * Utility - consists of common Utility functions which can be used across
 * Author: Ravichandran P
 */


import React, {Component} from 'react'
import {View, Text, Dimensions, Platform} from 'react-native'

var utilitySingletonObj = null

export default class Utility {
    constructor () {
        if (!utilitySingletonObj) {
            utilitySingletonObj = this
        }
        this.getDeviceDimensions = this.getDeviceDimensions.bind(this)
        this.getNavHeader = this.getNavHeader.bind(this)
        return utilitySingletonObj
    }

    /**
     * # getDeviceDimensions
     * Purpose: This function will be invoked to get dimensions of device
     * Author: Ravichandran P
     * Input Param:
     * Output Param: object
     */
    getDeviceDimensions () {
        let width = Dimensions.get('window').width
        let height = Dimensions.get('window').height
        return {width , height}
    }

    /**
     * # getNavHeader
     * Purpose: This function is called to render the Header view
     * Author: Ravichandran P
     * Input Param:
     * Output Param: object
     */
    getNavHeader () {
        return <View style={{height: 20, width: this.getDeviceDimensions().width, backgroundColor: "transparent"}}/>
    }
}