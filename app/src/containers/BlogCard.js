/**
* BlogCard.js deals with rendering of blog cards and pan action handling with the help of PanResponder
* Author: Ravichandran P
*/

import React, {Component} from 'react'
import {View, Text,StyleSheet, Animated, PanResponder, Image, Easing} from 'react-native'
import Utility from "../utilities/Utility"

var utilityObj = new Utility()
var deviceWidth = utilityObj.getDeviceDimensions().width
const DIRECTIONAL_DISTANCE_CHANGE_THRESHOLD = 0

export class BlogCard extends Component {
    constructor (props) {
        super (props)

        this.state = {
            blogData : props.blogData,
            translateX: new Animated.Value(0),
            animationType: "linear",
            isItDragged: false,
            prevCardIndex: 0
        }

        this.animatedValue = new Animated.Value(0)
        this.animatedSpinValue = new Animated.Value(0)
        this.springValue = new Animated.Value(0.3)
    }

    componentWillMount () {
        this.panResponder = PanResponder.create({
            onMoveShouldSetPanResponder: (e, gs) => this.handleOnMoveShouldSetPanResponder(e, gs),
            onPanResponderMove: (e, gs) => this.handlePanResponderMove(e, gs),
            onPanResponderRelease: (e, gs) => this.handlePanResponderEnd(e, gs),
        })
    }

    /**
     * # handleOnMoveShouldSetPanResponder
     * Purpose: This function will be invoked to enable the PanResponder & capture the gesture in X axis
     * Author: Ravichandran P
     * Input Param: e, gs
     * Output Param:
     */
    handleOnMoveShouldSetPanResponder (e, gs) {
        const { dx, dy } = gs
        let { cardIndex } = this.props
        let { prevCardIndex } = this.state
        if(cardIndex == 0)
            return Math.abs(dx) > DIRECTIONAL_DISTANCE_CHANGE_THRESHOLD
        else if(cardIndex == prevCardIndex+1)
            return Math.abs(dx) > DIRECTIONAL_DISTANCE_CHANGE_THRESHOLD
    }

    /**
     * # handlePanResponderMove
     * Purpose: This function is used to track gesture in X axis & to set state variable as per business logic
     * Author: Ravichandran P
     * Input Param: e, gestureState
     * Output Param:
     */
     handlePanResponderMove (e, gestureState) {
        const { dx } = gestureState
        const absDx = Math.abs(dx)
        this.setState({animationType: "linear", isItDragged: true})
        if(absDx > DIRECTIONAL_DISTANCE_CHANGE_THRESHOLD)
            this.setState({translateX: new Animated.Value(dx)})
    }

    /**
     * # handlePanResponderEnd
     * Purpose: This function will be invoked to handle the gesture at the end by PanResponder & to set state variable as per business logic
     * Author: Ravichandran P
     * Input Param: e, gestureState
     * Output Param:
     */
    handlePanResponderEnd (e, gestureState) {
        if(this.state.translateX._value > 80 || this.state.translateX._value < -80){
            this.props.updateBlogData(this.props.cardId)
            this.setState({translateX: new Animated.Value(0), animationType: "spin", isItDragged: true})
        }
        else {
            this.setState({translateX: new Animated.Value(0), animationType: "spring", isItDragged: false})
            this.animateSpring()
        }
    }

    render () {
        let data = this.props.blogData
        const spinView = this.animatedSpinValue.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '360deg']
        })
        const animationToBeApplied =  this.state.animationType !== "linear" ?
                                            (this.state.animationType === "spin" ? {transform: [{rotate: spinView}]} : {transform: [{scale: this.springValue}]})                                                :
                                                {transform: [ {translateX: this.state.translateX._value}]}
        const opacityVal =  this.state.animationType === "linear" && this.state.isItDragged ? 0.5 : 1
        
        return (
            <Animated.View style= {[ animationToBeApplied ,styles.swipeableRowFront,{opacity: opacityVal}]}  {...this.panResponder.panHandlers} >
                <View style = {[styles.swipeableRowFrontImgCntr,]} >
                    <Image
                        style={[styles.imageStyle]}
                        source={{uri: data.reqJsonImgSrc}}
                        resizeMode={'contain'} />
                </View>
                <View style = {[styles.contentView]}>
                    <Text numberOfLines = {1} style = {[styles.mainTxt]}>{data.title}</Text>
                    <Text style = {[styles.subTxt]}>{"Tags - "+data.tags}</Text>
                    <Text style = {[styles.subTxt]}>{"Author - "+data.author}</Text>
                    <Text style = {[styles.subTxt]}>{"Published at - "+data["published_at"]}</Text>
                </View>
            </Animated.View> )
    }

    /**
     * # animateSpring
     * Purpose: This function will be invoked to make the spring animation
     * Author: Ravichandran P
     * Input Param:
     * Output Param:
     */
    animateSpring () {
        this.springValue.setValue(0.9)
        Animated.spring(
            this.springValue,
            {
                toValue: 1,
                friction: 1
            }
        ).start()
    }

    /**
     * # animateSpin
     * Purpose: This function will be invoked to create the Spinning / Rotating animation
     * Author: Ravichandran P
     * Input Param:
     * Output Param:
     */
    animateSpin(){
        this.animatedSpinValue.setValue(0)
        Animated.timing(
            this.animatedSpinValue,
            {
                toValue: 1,
                duration: 1000,
                easing: Easing.ease,
                delay: 1000
            }
        )
    }

}

const styles = new StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
    },
    cardContainer: {
        justifyContent: 'center',
        width: deviceWidth,
        height: 260,
    },
    card: {
        justifyContent: 'center',
        width: deviceWidth,
        height: 260,
    },
    swipeableRowFront: {
        alignItems: 'center',
        backgroundColor: '#F2F2F2',
        justifyContent: 'center',
        height: 260,
        width: deviceWidth -50,
        flexDirection: 'column',
        borderWidth : 1,
        borderColor : 'grey',
        overflow: 'visible'
    },
    contentView: {
        width : deviceWidth -50,
        height : 80,
        flexDirection: 'column',
        justifyContent : 'flex-start',
        alignItems: 'flex-start',
        overflow: 'visible',
    },
    swipeableRowFrontImgCntr: {
        flexDirection : 'column',
        height: 180,
        borderBottomWidth:1,
    },
    swipeableRowBack: {
        flexDirection: 'row',
        height: 260,
        width: deviceWidth,
    },
    rowView: {
        height: 260,
        width : deviceWidth -50,
        backgroundColor: '#CCC',
    },
    filterSizeTxt :{
        color : "#06273F",
        paddingRight : 20,
        fontWeight : "normal",
        fontFamily : "Montserrat",
        fontSize : 18
    },
    selectedFilterSizeTxt : {
        backgroundColor: "#F6F6F6",
        fontWeight : "normal",
        fontFamily : "Montserrat",
        fontSize : 18,
        color : "#CACACA"
    },
    imageStyle: {
        width: deviceWidth -50,
        height: 140,
        marginTop : 20,
    },
    subTxt: {
        paddingLeft : 5,
        fontSize: 12,
        fontWeight :'normal',
        fontFamily : 'Cochin'
    },
    mainTxt: {
        paddingLeft : 5,
        paddingTop : 5,
        fontSize: 14,
        fontWeight :'600',
        fontFamily : 'Arial',

    }})

