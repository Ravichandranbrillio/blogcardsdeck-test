/**
 * FunkoBlog.js contains UI & Business logic for to develop a Deck of funko Blog Cards
 * Author: Ravichandran P
 */
import React, { Component } from 'react'
import {
    StyleSheet,
    View,
    Animated,
} from 'react-native'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {DOMParser} from 'react-native-html-parser'

import {actionCreators} from '../actions'
import {actionTypes} from '../actions/types'
import Utility from "../utilities/Utility"
import {fetchBlogArticleAction} from '../actions/FunkoBlog'
import {BlogCard} from "./BlogCard"

const utilityObj = new Utility()
var deviceWidth = utilityObj.getDeviceDimensions().width
const blogArticleUrl = "https://funko.com/ui-api/blog/5766367/articles"

class FunkoBlog extends Component {

    constructor (props) {
        super (props)
        this.state = {
            blogData: [],
            dimensionsSet: false,
            hiddenHeight: 0,
            hiddenWidth: 0,
            translateX: new Animated.Value(0),
        }
        this.imageArr = []
    }

    componentWillReceiveProps (nextProps, nextState) {
        if(nextProps.blogArticleRespProps !== this.props.blogArticleRespProps){
            let responseType = nextProps.blogArticleRespProps.type

            switch(responseType){
                case actionTypes.blogArticleSuccess :
                    this.blogArticleRespData = nextProps.blogArticleRespProps.response
                    for (blogArticle of this.blogArticleRespData) {
                            this.parseData(blogArticle)
                    }
                    (this.imageArr && Array.isArray(this.imageArr) && this.imageArr.length > 0) ?
                        this.setState({blogData : this.imageArr}) :
                            console.log("blogData source is not correctly parsed or prepared")
                    break

                case actionTypes.blogArticleFailure :
                        Alert("Something went wrong!!")
                    break
            }
        }
    }

    /**
     * # parseData
     * Purpose: This function will be invoked to parse dom and extract the required source like image,video etc.,
     * Author: Ravichandran P
     * Input Param: blogArticle
     * Output Param:
     */
    parseData (blogArticle) {
        let doc, domParser, imagesrc, videoSource
        let requiredJSONimag = blogArticle.image.src
        requiredJSONimag = requiredJSONimag.indexOf(".jpg?v=") > -1 ?
                            requiredJSONimag.slice(0, requiredJSONimag.indexOf(".jpg?v="))+"_large.jpg" :
                                requiredJSONimag.slice(0, requiredJSONimag.indexOf(".png?v="))+"_large.png"
        try{
            domParser = new DOMParser({
                errorHandler:{
                    locator : {},
                    error: function (err) {console.log("caught error : "+err)},
                }})
            doc = domParser.parseFromString( blogArticle.body_html,'text/html')
            imagesrc = doc.querySelect('img[src]')
            videoSource = doc.querySelect('iframe[src]')
            let zerothImageSrc = imagesrc[0].getAttribute("src").startsWith("https") ? imagesrc[0].getAttribute("src") : "https:"+imagesrc[0].getAttribute("src")
            imagesrc && imagesrc.length > 0 ? this.imageArr.push({...blogArticle, reqImgSrc: zerothImageSrc, reqJsonImgSrc: requiredJSONimag}) : console.log("image is not available")
        }
        catch (err) {
            console.log("parseData Error : ",err.message)
        }
    }

    componentDidMount () {
        this.props.fetchBlogArticleAction(blogArticleUrl)
    }

    /**
     * # updateBlogCard
     * Purpose: This function will be invoked to update the blogCards data once an card is removed
     * Author: Ravichandran P
     * Input Param: item
     * Output Param:
     */
    updateBlogCard (item) {
        let blogArray = this.state.blogData
        let updatedArray = []
        blogArray.filter((ele, index) => { ele.id != item ? updatedArray.push(ele) : () => {} })
        this.setState({blogData : updatedArray})
    }

    /**
     * # renderBlogCards
     * Purpose: This function will be invoked to render deck of Blog Cards
     * Author: Ravichandran P
     * Input Param:
     * Output Param: cards
     */
    renderBlogCards () {
       let inc = 0
       let zIndexInc = 0

       let cards = this.state.blogData.map((ele, index) => {
           if(index !== 0)
                inc  +=  2, zIndexInc += 9
           return <View style={[styles.rowView,{top: 5+inc, zIndex: 999-zIndexInc}]} key ={index}>
                       <View style = {[styles.swipeableRowBackCntr, {marginTop: index,}]}>
                           <BlogCard
                               blogData = {ele}
                               updateBlogData = {(ele)=>this.updateBlogCard(ele)}
                               cardId = {ele.id}
                               cardIndex = {index}
                               opacity ={index === this.state.blogData.length -2 ? 0.25 : 1} />
                       </View>
           </View>} )

        return cards
    }

    render () {
        return(
            <View style = {[styles.container]}>
                {utilityObj.getNavHeader()}
                <View style = {[styles.blogDeckContainer]}>
                    {this.renderBlogCards()}
                </View>
            </View> )
    }
}

const styles = new StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: '#CCC'
    },
    blogDeckContainer: {
        position: 'relative',
        marginTop : 20,
    },
    cardContainer: {
        justifyContent: 'center',
        width: deviceWidth,
        height: 260,
    },
    swipeableRowBackCntr: {
        flexDirection: 'row',
        height: 260,
        backgroundColor : 'transparent',
        width : deviceWidth - 50,
        overflow: 'visible'
    },
    rowView: {
        height: 260,
        width : deviceWidth -50,
        backgroundColor: 'transparent',
        position : 'absolute',
        alignSelf : 'center'
    },
})

function mapStateToProps (state) {
    return {
        blogArticleRespProps: state.funkoBlog
    }
}

function mapDispatchToProps (dispatch) {
    return bindActionCreators (actionCreators, dispatch)
}

export default connect (mapStateToProps, mapDispatchToProps) (FunkoBlog)