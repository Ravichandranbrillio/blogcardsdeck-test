/**
 * singleStore.js - creates a store by applying middleware and export it out
 * Author: Ravichandran P
 */

import {applyMiddleware, createStore} from 'redux'
import {Provider} from 'react-redux'
import thunkMiddleware from 'redux-thunk'
import rootReducer from '../reducers/index'

const createStoreWithMiddleware = applyMiddleware(thunkMiddleware)(createStore)
export const store = createStoreWithMiddleware(rootReducer)

