/**
 * App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react'
import {Provider} from 'react-redux'
import FunkoBlog from './app/src/containers/FunkoBlog'
import {store} from './app/src/store/singleStore'

export default class App extends Component{
  render() {
      return (<Provider store = {store}>
                    <FunkoBlog />
              </Provider>)
  }
}
