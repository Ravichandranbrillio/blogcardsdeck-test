## MOB 2018 - Blog Deck 
---
1. Clone the **git repository** by using command **git clone https://Ravichandranbrillio@bitbucket.org/Ravichandranbrillio/blogcardsdeck-test.git** 
2. Run the command **npm install** 
3. Go to ios directory and open *BlogCardsDeck.xcodeproj* format file in xcode
4. Clean and Build the project
5. Once the Application build is successful, Select a simulator/ connected device and run the Application .
